jQuery(document).ready(function($) {

	/*
	tiny-slider Carousel (https://ganlanyuan.github.io/tiny-slider/)
	*/
	if ($('.slider').length) {
		var slider = tns({
			container: '.slider',
			items: 3,
			fixedWidth: 375,
			autoplay: true,
			autoplayHoverPause: true,
			speed: 800,
			mouseDrag: true,
			controls: false,
			nav: false,
			autoplayButtonOutput: false
		});

		console.log("carousel loaded");

		/*
		carousel expander/retractor
		*/
		$(".expander").click(function() {
			$("#primary.carousel").toggleClass("close");
		});
	}
	
	/*
    Activate youtube buttons
	*/
	var youtube = document.querySelectorAll( ".youtube" );
	for (var i = 0; i < youtube.length; i++) { 
	    youtube[i].addEventListener( "click", function() {
	 
	        var iframe = document.createElement( "iframe" );
	 
            iframe.setAttribute( "frameborder", "0" );
            iframe.setAttribute( "allowfullscreen", "" );
            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
 
            this.innerHTML = "";
            this.appendChild( iframe );

            $(this).prev().remove();
	    } );
	}
});
