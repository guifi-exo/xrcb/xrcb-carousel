<?php
/**
 * Template Name: XRCB Carousel
 *
 * @package xrcb
 */

$showLastEvent = true;
$showLastPodcast = true;
$showRandomVideo = false;

get_header(); ?>

	<div id="primary" class="content-area carousel">

		<?php
			/* all slides*/
			$args = array(
				'post_type' => 'slide',
				'post_status' => 'publish',
				'posts_per_page' => '10',
				'order' => 'ASC',
				'orderby' => 'ID',
			);

			$my_query = new WP_Query($args);
		?>

		<div id="content" class="site-content" role="main">

			<div class="carousel slider">

			<?php

			// Get all posts from content type "slide"

			if ( $my_query->have_posts() ) {

				while ($my_query->have_posts()) {

					$my_query->the_post();

					$type = get_post_meta(get_the_ID(), 'categoria', true);
				?>

				<div class="slide <?php echo $type; ?>">

					<h5 class="category"><?php echo get_post_meta(get_the_ID(), 'categoria', true); ?></h5>

					<?php
						if ($type == "video" || $type == "podcast" || $type == "news" || $type == "event") :
					?>

					<header class="entry-header">
						<h2 class="entry-title">
							<?php
								$link = get_post_meta(get_the_ID(), 'url_link', true);
								if ($link != '')
									echo "<a href='".$link."'>".get_the_title()."</a>";
								else
									echo get_the_title();
							?>
						</h2>
						<p class="entry-title-meta"><?php echo wpm_translate_string(get_post_meta(get_the_ID(), 'subtitle', true)); ?></p>
					</header><!-- .entry-header -->

					<?php endif; ?>

					<?php
						if ($type == "video") :
					?>

						<div class="entry-content youtube" data-embed="<?php echo substr(get_post_meta(get_the_ID(), 'url_video', true), strlen('https://www.youtube.com/watch?v=')); ?>">
							<?php if ( has_post_thumbnail() ) the_post_thumbnail('medium'); ?>
							<div class="play-button"></div>
						</div>

					<?php
						elseif ($type == "podcast") :
					?>

						<?php
							$podcast_id = get_post_meta(get_the_ID(), 'podcast', true);

							if ($podcast_id > 0) :
						?>

							<?php if ( has_post_thumbnail() ) : ?>

								<div class="entry-content" style="background: center / cover no-repeat url(' <?php the_post_thumbnail_url('medium'); ?> ');">

							<?php else : ?>

								<div class="entry-content">

							<?php endif; ?>

								<div class="btn-play-container">
									<div class='btn btn-play btn-play-inverse piwik_download' data-src='<?php echo wp_get_attachment_url(get_post_meta($podcast_id, 'file_mp3', true)); ?>' data-radio='<?php echo get_the_title(get_post_meta($podcast_id, 'radio', true)); ?>' data-title='<?php the_title(); ?>' data-radio-link='<?php echo get_the_permalink(get_post_meta($podcast_id, 'radio', true)); ?>' data-podcast-link='<?php echo get_the_permalink($podcast_id); ?>' data-programa='<?php echo get_the_title(get_post_meta($podcast_id, 'programa', true)); ?>'></div>
								</div>
							</div>

						<?php endif; ?>

					<?php
						elseif ($type == "event" || $type == "news") :
					?>

						<?php if ( has_post_thumbnail() ) : ?>

							<?php
								$link = get_post_meta(get_the_ID(), 'url_link', true);
								if ($link != '') echo "<a href='".$link."'>";
							?>

							<div class="entry-content" style="background: center / cover no-repeat url(' <?php the_post_thumbnail_url('medium'); ?> ');"></div>

							<?php
								if ($link != '') echo "</a>";
							?>

						<?php else : ?>

							<div class="entry-content"></div>

						<?php endif; ?>

					<?php
						elseif ($type == "tweet") :
					?>

						<div class="entry-content">
							<?php echo get_the_content(); ?>
						</div>

					<?php endif; ?>

				</div>

			<?php }

			}

			if ($showLastEvent) : ?>

				<div class="slide event">

					<h5 class="category">último evento</h5>

					<?php
						/* last event */
						$event_args = array(
							'post_type' => 'post',
							'post_status' => 'publish',
							'posts_per_page' => '1',
							'orderby' => 'date' ,
							'order' => 'DESC' ,
					    	'tax_query' => array(
						        array(
						            'taxonomy' => 'category',
						            'field'    => 'slug',
						            'terms'    => 'event',
						        ),
						    ),
						);

						$event_query = new WP_Query($event_args);

						if ( $event_query->have_posts() ) : ?>

							<?php $event_query->the_post(); ?>

							<header class="entry-header">
								<h2 class="entry-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>
								<p class="entry-title-meta">
									<?php
										$excerpt = get_the_excerpt();
										$excerpt = substr($excerpt, 0, 160);
										$result = substr($excerpt, 0, strrpos($excerpt, ' '));
										echo $result."...";
									?>
								</p>
							</header><!-- .entry-header -->

							<?php if ( has_post_thumbnail() ) : ?>

								<a href='<?php the_permalink(); ?>'>

								<div class="entry-content" style="background: center / cover no-repeat url(' <?php the_post_thumbnail_url('medium'); ?> ');"></div>
								</a>

							<?php else : ?>

								<div class="entry-content"></div>

							<?php endif; ?>

						<?php endif; ?>

				</div><!-- .event -->

			<?php endif; ?>

			<?php if ($showLastPodcast) : ?>

				<div class="slide podcast">

					<h5 class="category">último podcast</h5>

				<?php
					/* last podcast */
					$podcast_args = array(
						'post_type' => 'podcast',
						'post_status' => 'publish',
						'posts_per_page' => '1',
						'orderby' => 'date' ,
						'order' => 'DESC' ,
				    	'meta_query' => array(
							array(
								'relation' => 'OR',
								array(
									'key'     => 'live',
									'compare' => 'NOT EXISTS',
								),
								array(
									'key'     => 'live',
									'value'   => 'true',
									'compare' => '!=',
								),
							),
						),
					);

					$podcast_query = new WP_Query($podcast_args);

					if ( $podcast_query->have_posts() ) {

						// load last podcast
						$podcast_query->the_post();
						$radio = get_post_meta(get_the_ID(), 'radio', true);
						?>

						<header class="entry-header">
							<h2 class="entry-title">
								<?php
									echo "<a href='".get_permalink()."'>".get_the_title()."</a>";
								?>
							</h2>
							<p class="entry-title-meta"><?php echo get_the_title($radio); ?></p>
						</header><!-- .entry-header -->

						<?php 
							if (get_field('img_podcast')) {
								$thumbnail = get_field('img_podcast')['url'];
							}
							else {
								$thumbnail = get_the_post_thumbnail_url($radio, 'medium');
							}

							if ( $thumbnail ) : ?>

							<div class="entry-content" style="background: center / cover no-repeat url(' <?php echo $thumbnail; ?> ');">

							<?php else : ?>

								<div class="entry-content">

							<?php endif; ?>

							<div class="btn-play-container">
								<div class='btn btn-play piwik_download' data-src='<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), 'file_mp3', true)); ?>' data-radio='<?php echo get_the_title(get_post_meta(get_the_ID(), 'radio', true)); ?>' data-title='<?php the_title(); ?>' data-radio-link='<?php echo get_the_permalink(get_post_meta(get_the_ID(), 'radio', true)); ?>' data-podcast-link='<?php echo get_the_permalink(get_the_ID()); ?>' data-programa='<?php echo get_the_title(get_post_meta($podcast_id, 'programa', true)); ?>'></div>
							</div>
						</div>

					<?php } ?>

				</div><!-- .podcast -->

			<?php endif; ?>

			<?php if ($showRandomVideo) : ?>

				<div class="slide video">

					<h5 class="category">research</h5>

				<?php
					/* all slides*/
					$video_args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => '1',
						'category_name' => 'research',
						'tag' => 'video',
  						'orderby' => 'rand',
					);

					$video_query = new WP_Query($video_args);

					if ( $video_query->have_posts() ) {

						// load 1 random podcast
						$video_query->the_post();
						?>

						<header class="entry-header">
							<h2 class="entry-title">
								<?php
									echo "<a href='".get_permalink()."'>".get_the_title()."</a>";
								?>
							</h2>
							<p class="entry-title-meta"><?php echo strip_tags(get_the_category_list( __( ' / ', 'xrcb' ) ) ); ?></p>
						</header><!-- .entry-header -->

						<?php
							// get youtube url from content
							$youtube_url = "https://www.youtube.com/embed/";
							$video_pos = strpos(get_the_content(), $youtube_url) + strlen($youtube_url);

							if ($video_pos >= 0) :
								$video_final_pos = strpos(get_the_content(), " ", $video_pos);
								if (!$video_final_pos) {
									// video url is at end of content
									$video_final_pos = strlen(get_the_content());
								}
								$video = substr(get_the_content(), $video_pos, $video_final_pos-$video_pos);
								?>

								<div class="entry-content youtube" data-embed="<?php echo $video; ?>">
									<?php if ( has_post_thumbnail() ) the_post_thumbnail('medium'); ?>
									<div class="play-button"></div>
								</div>

						<?php
							endif;
						?>

					<?php } ?>

				</div><!-- .video -->

			<?php endif; ?>

			</div><!-- .carousel -->

			<div class="expander"></div>

		</div><!-- #content -->

	</div><!-- #primary -->

<?php get_footer(); ?>
