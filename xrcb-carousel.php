<?php
/*
Plugin Name: XRCB Carousel
Description: Carousel of slides for XRCB front page
Author: Gerald Kogler
Author URI: http://go.yuri.at
Text Domain: xrcb-carousel
*/

wp_enqueue_style('xrcb-carousel-css', plugins_url('/carousel.css', __FILE__ ), array(), time() );
wp_enqueue_style('tiny-slider-css', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css');
wp_enqueue_script('jquery');
wp_enqueue_script('tiny-slider', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/min/tiny-slider.js');
wp_enqueue_script('xrcb-carousel-js', plugins_url('/carousel.js', __FILE__ ));

/* load textdomain */
add_action( 'init', 'xrcb_carousel_load_textdomain' );
function xrcb_carousel_load_textdomain() {
	load_plugin_textdomain('xrcb-carousels', false, dirname(plugin_basename(__FILE__)));
}

// custom post type Slide
function xrcb_carousel_register_slide() {

	/**
	 * Post Type: Slides.
	 */

	$labels = array(
		"name" => __( "Slides", "xrcb" ),
		"singular_name" => __( "Slide", "xrcb" ),
	);

	$args = array(
		"label" => __( "Slides", "xrcb" ),
		"labels" => $labels,
		"description" => "xrcb slide",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "slides",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "slide",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "slide", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-format-video",
		"supports" => array( "title", "editor", "author", "thumbnail" ),
	);

	register_post_type( "slide", $args );
}

add_action( 'init', 'xrcb_carousel_register_slide' );

// ACF field group export
//include_once('advanced-custom-fields/acf.php');
if(function_exists("register_field_group")) {
	register_field_group(array (
		'id' => 'acf_slide',
		'title' => 'Slide',
		'fields' => array (
			array (
				'key' => 'field_5a69b5692cccc',
				'label' => 'Categoría',
				'name' => 'categoria',
				'type' => 'radio',
				'choices' => array (
					'video' => 'Vídeo',
					'podcast' => 'Podcast',
					'news' => 'Noticia',
					'tweet' => 'Tweet',
					'event' => 'Evento',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a685a93f9uuu',
				'label' => 'Subtítulo',
				'name' => 'subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '100',
				//'required' => 1,
				'conditional_logic' => [
                    [
                        [
                            "field" => "field_5a69b5692cccc",
                            "operator" => "!=",
                            "value" => "tweet"
                        ]
                    ]
                ],
			),
			array (
				'key' => 'field_5a685a93f9www',
				'label' => 'URL vídeo',
				'name' => 'url_video',
				'type' => 'url',
				'conditional_logic' => [
                    [
                        [
                            "field" => "field_5a69b5692cccc",
                            "operator" => "==",
                            "value" => "video"
                        ]
                    ]
                ],
				'required' => 1,
			),
			array (
				'key' => 'field_5b2a14e0b2qqq',
				'label' => 'Podcast',
				'name' => 'podcast',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'podcast',
				),
				'multiple' => 0,
				'conditional_logic' => [
                    [
                        [
                            "field" => "field_5a69b5692cccc",
                            "operator" => "==",
                            "value" => "podcast"
                        ]
                    ]
                ],
				'required' => 1,
			),
			array (
				'key' => 'field_5a685a93f9fff',
				'label' => 'URL link',
				'name' => 'url_link',
				'type' => 'url',
				'required' => 0,
				'conditional_logic' => [
                    [
                        [
                            "field" => "field_5a69b5692cccc",
                            "operator" => "!=",
                            "value" => "tweet"
                        ]
                    ]
                ],
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'slide',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

/* add capability file upload to users */
function xrcb_carousel_add_cap_slide() {
	// admin
	$role = get_role( 'administrator' );
    $role->add_cap( 'edit_slides' ); 
    $role->add_cap( 'edit_published_slides' ); 
    $role->add_cap( 'edit_private_slides' ); 
    $role->add_cap( 'edit_others_slides' ); 
    $role->add_cap( 'publish_slides' ); 
    $role->add_cap( 'read_private_slides' ); 
    $role->add_cap( 'delete_private_slides' ); 
    $role->add_cap( 'delete_published_slides' ); 
    $role->add_cap( 'delete_others_slides' ); 

    // editor
	$role = get_role( 'editor' );
    $role->add_cap( 'edit_slides' ); 
    $role->add_cap( 'edit_published_slides' ); 
    $role->add_cap( 'edit_private_slides' ); 
    $role->add_cap( 'edit_others_slides' ); 
    $role->add_cap( 'publish_slides' ); 
    $role->add_cap( 'read_private_slides' ); 
    $role->add_cap( 'delete_private_slides' ); 
    $role->add_cap( 'delete_published_slides' ); 
    $role->add_cap( 'delete_others_slides' );
}
add_action( 'init', 'xrcb_carousel_add_cap_slide' );

add_action('template_include', 'xrcb_carousel_template_include');
function xrcb_carousel_template_include($template) {
	if ( is_page( 'front-slider' )  ) {
	    $file = dirname( __FILE__ ).'/xrcb-carousel-template.php';
	    if(file_exists($file)) {
	        $template = $file;
	    }
	}

    return $template;
}

?>
