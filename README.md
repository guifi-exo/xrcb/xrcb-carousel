# FREEZED branch.

Do not use this branch, please go to [prod](https://gitlab.com/guifi-exo/xrcb/xrcb-carousel/-/tree/prod)

# xrcb-carousel

carousel for [xrcb.cat wordpress theme](https://gitlab.com/guifi-exo/xrcb.cat)

This shows latest podcasts, news, live, etc.
